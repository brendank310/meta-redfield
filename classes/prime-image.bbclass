inherit image

IMAGE_LINGUAS = "en-us"

IMAGE_INSTALL = "\
    base-files \
    base-passwd \
    bind-utils \
    coreutils \
    lsb \
    gawk \
    grep \
    iproute2 \
    iputils \
    iputils-arping \
    iputils-clockdiff \
    iputils-ping \
    iputils-tracepath \
    iputils-traceroute6 \
    nano \
    netbase \
    rng-tools \
    sed \
    shadow \
    systemd \
    systemd-compat-units \
    vim-tiny \
    ${MACHINE_ESSENTIAL_EXTRA_RDEPENDS} \
"

IMAGE_FEATURES = " \
    read-only-rootfs \
    empty-root-password \
    allow-empty-password \
"

ROOTFS_RO_UNNEEDED = "update-rc.d ${VIRTUAL-RUNTIME_update-alternatives} ${ROOTFS_BOOTSTRAP_INSTALL}"

#########################
# DEFAULT POSTPROCESS FUNCTIONS
#########################

image_postprocess_symlink_usr_ld_linux() {
    # XXX: required by iproute2? must be a bug somewhere...
    ln -sf /lib/ld-linux-x86-64.so.2 ${IMAGE_ROOTFS}/usr/lib/ld-linux-x86-64.so.2
}

image_postprocess_default_image_timestamp() {
    echo "IMAGE:${IMAGE_BASENAME}" > ${IMAGE_ROOTFS}/etc/prime-version
    echo "DATE:$(DATE)" >> ${IMAGE_ROOTFS}/etc/prime-version
}

image_postprocess_default_image_hostname() {
    echo "${IMAGE}" > ${IMAGE_ROOTFS}/etc/hostname
    echo "local" > ${IMAGE_ROOTFS}/etc/domainname
}

image_postprocess_purge_default_certificates() {
    rm -rf ${IMAGE_ROOTFS}/usr/share/ca-certificates/*
    rm -rf ${IMAGE_ROOTFS}/etc/ssl/certs/*
}

image_postprocess_override_network_online_target_timeout() {
    mkdir -p ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d
    echo "[Service]" > ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
    echo "ExecStart=" >> ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
    echo "ExecStart=/lib/systemd/systemd-networkd-wait-online --timeout=5" >> ${IMAGE_ROOTFS}/etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
}

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_symlink_usr_ld_linux; \
    image_postprocess_default_image_timestamp; \
    image_postprocess_default_image_hostname; \
    image_postprocess_purge_default_certificates; \
    image_postprocess_override_network_online_target_timeout; \
"

#########################
# SHARED (SELECTABLE) POSTPROCESS FUNCTIONS
#########################

image_postprocess_static_resolved() {
    # resolved static mode (points to localhost) - required for anyconnect w/o proper networked dns
    ln -sf /run/systemd/resolve/stub-resolv.conf ${IMAGE_ROOTFS}${sysconfdir}/resolv.conf
}

image_postprocess_tmpfs_root_home() {
    echo "tmpfs /home/root tmpfs defaults 0  0" >> ${IMAGE_ROOTFS}/etc/fstab
}

image_postprocess_divert_iptables() {
    mv ${IMAGE_ROOTFS}/usr/sbin/iptables ${IMAGE_ROOTFS}/usr/sbin/iptables.disabled
}

#########################
# DEBUG LOVE
#########################

IMAGE_FEATURES += " \
    debug-tweaks \
    package-management \
"

IMAGE_INSTALL += " \
    binutils \
    curl \
    file \
    findutils \
    gdb \
    gzip \
    ldd \
    less \
    lsof \
    openssh \
    opkg \
    opkg-utils \
    strace \
    tcpdump \
    pciutils \
    procps \
    psmisc \
    rsync \
    tar \
    usbutils \
    wget \
    which \
    zlib \
    xz \
"

sshd_permit_empty_password() {
    sed -i 's|#PermitEmptyPasswords no|PermitEmptyPasswords yes|g' ${IMAGE_ROOTFS}${sysconfdir}/ssh/sshd_config
}

ROOTFS_POSTPROCESS_COMMAND_append = " \
    sshd_permit_empty_password; \
"

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "" ,d)}"
