SUMMARY = "A minimalist nat ndvm"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit prime-image

IMAGE_INSTALL += " \
    prime-image-configs \
    packagegroup-network-driver-modules \
    packagegroup-networkvm \
    ndvm-manager \
    ndvm-iptables-configs \
    ndvm-networkd-configs \
    ndvm-sysctl-configs \
    ndvm-modules-configs \
    kernel-module-xen-blkback \
    kernel-module-xen-gntalloc \
    kernel-module-xen-gntdev \
    kernel-module-xen-netback \
    kernel-module-xen-wdt \
    linux-firmware \
"
