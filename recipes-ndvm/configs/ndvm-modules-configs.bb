DESCRIPTION = "NDVM SYSCTL CONFIGS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://10-iwlwifi.conf \
"

FILES_${PN} += "/etc/modules-load.d/10-iwlwifi.conf"

do_install() {
    install -d ${D}${sysconfdir}/modules-load.d
    install -m 0644 ${WORKDIR}/10-iwlwifi.conf ${D}${sysconfdir}/modules-load.d
}

RDEPENDS_${PN} = " \
"
