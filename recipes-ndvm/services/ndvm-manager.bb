DESCRIPTION = "NDVM TOOL(STACK)"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

SRC_URI = " \
    file://ndvm-manager \
    file://ndvm-manager.service \
"

do_install() {
    install -d ${D}${sbindir}
    install -m 0755 ${WORKDIR}/ndvm-manager ${D}${sbindir}

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/ndvm-manager.service ${D}${systemd_system_unitdir}
}

RDEPENDS_${PN} = " \
    bash \
    ethtool \
    wpa-supplicant \
"

SYSTEMD_SERVICE_${PN} = "ndvm-manager.service"
