SUMMARY = "Desirable packages for network vms (common between ndvm, vpnvm, etc.)"
LICENSE = "MIT"

inherit packagegroup

RDEPENDS_${PN} = " \
    bridge-utils \
    conntrack-tools \
    coreutils \
    ethtool \
    iptables \
    packagegroup-netfilter \
    kernel-module-xen-acpi-processor \
    kernel-module-xen-gntalloc \
    kernel-module-xen-netback \
    kernel-module-xen-netfront \
    kernel-module-xen-wdt \
    pciutils \
    usbutils \
    xen-devd \
    xen-xenstore \
    xen-scripts-network \
    xen-xencommons \
"

# DEBUG LOVE
RDEPENDS_${PN} += " \
    tcpdump \
"
