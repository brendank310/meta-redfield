FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit systemd

SRC_URI += " \
    file://iptables.service \
    file://ip6tables.service \
"

do_install_append() {
    install -d -m 755 ${D}${systemd_system_unitdir}
    install -p -m 644 ${WORKDIR}/iptables.service ${D}${systemd_system_unitdir}
    install -p -m 644 ${WORKDIR}/ip6tables.service ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = "iptables.service ip6tables.service"
FILES_${PN} += "${systemd_system_unitdir}"
