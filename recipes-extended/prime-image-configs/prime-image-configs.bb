SUMMARY = "Common configs used in prime images"
DESCRIPTION = "Common configs used in prime images"
SECTION = "images"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = " \
    file://var-volatile-log-xen.conf \
"

S = "${WORKDIR}"

do_install () {
	# /var/log/xen mount point for xen/devd
	install -d -m 755 ${D}${sysconfdir}/tmpfiles.d
	install -p -m 644 var-volatile-log-xen.conf ${D}${sysconfdir}/tmpfiles.d/
}

#FILES_${PN} = "${sysconfdir}/"
