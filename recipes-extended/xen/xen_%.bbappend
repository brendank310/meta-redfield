FILESEXTRAPATHS_prepend := "${THISDIR}/:"

SRC_URI += " \
    file://defconfig \
    file://xendriverdomain-hack.patch \
"

INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP_${PN} = "already-stripped"
INSANE_SKIP_${PN}-hvmloader = "arch"
INSANE_SKIP_${PN}-hvmloader += " already-stripped "
