SUMMARY = "Tools to interact with ext filesystems"

DESCRIPTION = "Utilities to interact with ext filesystems without explicit mounting"

HOMEPAGE = "http://home.earthlink.net/~k_sheff/sw/e2tools"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=fa8321a71778d26ff40690a4d371ea85"

SRC_URI = "http://home.earthlink.net/~k_sheff/sw/e2tools/${PN}-${PV}.tar.gz \
	   file://e2tools-0.0.16-prototypes.patch \
	   file://e2tools-0.0.16-qsort-const.patch \
           "

DEPENDS += "e2fsprogs"

SRC_URI[md5sum] = "1829b2b261e0e0d07566066769b5b28b"
SRC_URI[sha256sum] = "4e3c8e17786ccc03fc9fb4145724edf332bb50e1b3c91b6f33e0e3a54861949b"

inherit autotools 
