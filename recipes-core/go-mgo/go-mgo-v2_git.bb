SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=566e96676859b5704130b80941bc9f1f"

inherit go

GO_IMPORT = "gopkg.in/mgo.v2"
SRC_URI = "git://${GO_IMPORT};protocol=git;branch=v2;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"
SRCREV="3f83fa5005286a7fe593b055f0d7771a7dce4655"

RDEPENDS_${PN}-dev = "bash make"
DEPENDS += " \
	go-tomb \
	cyrus-sasl \
	"
