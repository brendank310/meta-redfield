SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=566e96676859b5704130b80941bc9f1f"

inherit go

GO_IMPORT = "github.com/globalsign/mgo"
SRC_URI = "git://${GO_IMPORT};protocol=git;branch=master;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"
SRCREV="${AUTOREV}"

RDEPENDS_${PN}-dev = "bash make"
DEPENDS += " \
	go-tomb \
	cyrus-sasl \
	"
