SUMMARY = "Go tools (subset)"
HOMEPAGE = "https://godoc.org/golang.org/x/tools"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${GO_IMPORT}/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707"
SRC_URI = "git://github.com/golang/tools;name=tools \
           git://github.com/golang/net;name=net;destsuffix=deps/golang.org/x/net"

require golang-x-srcrevs.inc

SRCREV_tools = "${GOLANG_X_TOOLS_SRCREV}"
SRCREV_net = "${GOLANG_X_NET_SRCREV}"
SRCREV_FORMAT = "tools+net"
PV="1.0+git${SRCPV}"

GO_IMPORT = "golang.org/x/tools"

DEPENDS = "golang-x-crypto"

inherit go

PTEST_ENABLED = ""

RDEPENDS_${PN}-dev = "bash"
INSANE_SKIP_${PN}-dev += "arch"

BBCLASSEXTEND = "native"

do_compile_prepend() {
        mv ${WORKDIR}/deps/golang.org/x/* ${S}/src/golang.org/x/
}

