SUMMARY = "A go library"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=fb8b39492731abb9a3d68575f3eedbfa"
SRC_URI = "git://${GO_IMPORT};protocol=git;branch=master"
SRCREV="${AUTOREV}"

inherit go

GO_IMPORT = "github.com/PuerkitoBio/purell"

RDEPENDS_${PN}-dev = "bash make"

DEPENDS += " \
	golang-x-net \
	golang-x-text \
	go-puerkitobio-urlesc \
	"
