SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

inherit go

GO_IMPORT = "github.com/go-openapi/swag"
SRC_URI = "git://${GO_IMPORT};protocol=git;branch=master;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"
SRCREV="${AUTOREV}"

RDEPENDS_${PN}-dev = "bash make"
DEPENDS += " \
	go-govalidator \
	go-openapi-errors \
        go-easyjson \
        go-mapstructure \
	go-mgo-v2 \
	go-yaml \
	"

