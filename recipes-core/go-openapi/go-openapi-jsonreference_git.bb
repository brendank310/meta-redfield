SUMMARY = "A go library"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"
SRC_URI = "git://github.com/go-openapi/jsonreference.git;protocol=git;branch=master"
SRCREV="${AUTOREV}"

inherit go

GO_IMPORT = "github.com/go-openapi/jsonreference"

RDEPENDS_${PN}-dev = "bash make"

DEPENDS += " \
	go-openapi-jsonpointer \
	go-puerkitobio-purell \
	"
