SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${GO_IMPORT}/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"
SRC_URI = "git://github.com/go-openapi/errors.git;protocol=git;branch=master"
SRCREV="${AUTOREV}"

inherit go

GO_IMPORT = "github.com/go-openapi/errors"

RDEPENDS_${PN}-dev = "bash make"
