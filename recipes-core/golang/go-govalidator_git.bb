SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${GO_IMPORT}/LICENSE;md5=9548240229052f3a5f5bdf14ac19bbe3"
SRC_URI = "git://github.com/asaskevich/govalidator.git;protocol=git;branch=master"
SRCREV="${AUTOREV}"

inherit go

GO_IMPORT = "github.com/asaskevich/govalidator"

RDEPENDS_${PN}-dev = "bash make"

