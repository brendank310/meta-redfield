SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${GO_IMPORT}/LICENSE.txt;md5=7b87248414394a9c3768f838b355b5ea"
SRC_URI = "git://github.com/devfacet/gocmd.git;protocol=git"
SRCREV="v${PV}"

inherit go

GO_IMPORT = "github.com/devfacet/gocmd"

RDEPENDS_${PN}-dev = "bash make"

