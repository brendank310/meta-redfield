SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"
GO_IMPORT = "github.com/mailru/easyjson"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=819e81c2ec13e1bbc47dc5e90bb4d88b"
SRC_URI = "git://${GO_IMPORT};protocol=https;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"

SRCREV="${AUTOREV}"

inherit go

RDEPENDS_${PN}-dev = "bash make"

