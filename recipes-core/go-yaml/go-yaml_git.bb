SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=95d4102f39f26da9b66fee5d05ac597b"

inherit go

GO_IMPORT = "gopkg.in/yaml.v2"
SRC_URI = "git://git@${GO_IMPORT};protocol=git;branch=v2;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"
SRCREV="5420a8b6744d3b0345ab293f6fcba19c978f1183"

RDEPENDS_${PN}-dev = "bash make"

