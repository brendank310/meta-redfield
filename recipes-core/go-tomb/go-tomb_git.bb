SUMMARY = "A library for implementing command line interfaces in golang"
HOMEPAGE = "http://www.github.com/devfacet/gocmd"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=95d4102f39f26da9b66fee5d05ac597b"

inherit go

GO_IMPORT = "gopkg.in/tomb.v2"
SRC_URI = "git://github.com/go-tomb/tomb;protocol=git;branch=v2;destsuffix=${PN}-${PV}/src/${GO_IMPORT}"
SRCREV="d5d1b5820637886def9eef33e03a27a9f166942c"

RDEPENDS_${PN}-dev = "bash make"

