DESCRIPTION = "A minimal xen image"

INITRD_IMAGE = "dom0-image-initramfs"

inherit prime-image

IMAGE_INSTALL += " \
    prime-image-configs \
    installer-iptables-configs \
    installer-networkd-configs \
    installer-sysctl-configs \
    installer-tmpfs-configs \
    packagegroup-network-driver-modules \
    packagegroup-networkvm \
    packagegroup-storage-driver-modules \
    kernel-module-i915 \
    packagegroup-fonts-truetype-core \
    pciutils \
    coreutils \
    usbutils \
    plymouth \
    wpa-supplicant \
    wireless-regdb \
    linux-firmware \
    parted \
    dosfstools \
    lvm2 \
    e2fsprogs-mke2fs \
    kernel-modules \
    "

LICENSE = "MIT"
EFI_PROVIDER = "grub-efi"

image_postprocess_installer() {
    mkdir -p ${IMAGE_ROOTFS}/images
    mkdir -p ${IMAGE_ROOTFS}/storage
}

IMAGE_ROOTFS_EXTRA_SPACE += " + 32768 "

ROOTFS_POSTPROCESS_COMMAND_append = " \
    image_postprocess_installer; \
"

IMAGE_FSTYPES += "cpio.gz"
